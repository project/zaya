## INTRODUCTION
Zaya is a project to engage self learning based on itineraries.
An itinerary is a self learning tool consisting of an ordered succession of
chapters, contextualized and structured by a story that makes it easier for
the learner to understand the course objectives.

### Features

Based on group to provide learning itineraries. It will let progress based on
the milestones defined by the itinerary editor (read, interact, submit a form
...)

The primary use case for this module is:

- Let users follow by himself (or with minimal help) the learning itinerary.
Itinerary has chapters. Chapters has resources. Resources has media and/or
forms.

## REQUIREMENTS

Group, but is included in composer.json and info.yml

## INSTALLATION

The complete zaya project site could be installed as explained in 

https://gitlab.com/communalia/zaya_project_site/-/blob/1.0.x/README.md

For a reduced zaya project without user managing modules use this recipe:

https://www.drupal.org/project/zaya_project

If you prefer to install module manually, install as you would normally 
install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Go to groups to create your first itinerary.

## MAINTAINERS

Current maintainers for Drupal 10:

- Aleix Quintana (aleix) - https://www.drupal.org/u/aleix
