<?php

namespace Drupal\zaya_events\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupMembershipInterface;
use Drupal\group\Entity\GroupRelationshipInterface;

/**
 * Event that is fired when a user logs in.
 */
class ChapterCompletionEvent extends Event {

  // This makes it easier for subscribers to reliably use our event name.
  const EVENT_NAME = 'chapter_completion';

  /**
   * The user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  public $account;

  /**
   * The membership account.
   *
   * @var \Drupal\group\Entity\GroupMembershipInterface
   */
  public $groupMembership;

  /**
   * The relationship to chapter content completed.
   *
   * @var \Drupal\group\Entity\GroupRelationshipInterface
   */
  public $groupChapterRelationship;

  /**
   * The type of action done that made this chapter completed.
   *
   * @var string
   */
  public $completionAction;

  /**
   * Constructs the event.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account of the user logged in.
   * @param \Drupal\group\Entity\GroupMembershipInterface $group_membership
   *   The membership entity.
   * @param \Drupal\group\Entity\GroupRelationshipInterface $group_chapter_relationship
   *   The relationship between group and chapter completed.
   * @param string $completion_action
   *   The name of the completion action.
   */
  public function __construct(AccountInterface $account, GroupMembershipInterface $group_membership, GroupRelationshipInterface $group_chapter_relationship, $completion_action) {
    $this->account = $account;
    $this->groupMembership = $group_membership;
    $this->groupChapterRelationship = $group_chapter_relationship;
    $this->completionAction = $completion_action;
  }

}
