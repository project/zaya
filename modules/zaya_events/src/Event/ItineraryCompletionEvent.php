<?php

namespace Drupal\zaya_events\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupMembershipInterface;
use Drupal\group\Entity\GroupRelationshipInterface;

/**
 * Event that is fired when itinerary is marked as completed.
 */
class ItineraryCompletionEvent extends Event {

  // This makes it easier for subscribers to reliably use our event name.
  const EVENT_NAME = 'itinerary_completion';

  /**
   * The user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  public $account;

  /**
   * The membership account.
   *
   * @var \Drupal\group\Entity\GroupMembershipInterface
   */
  public $groupMembership;

  /**
   * The itinerary group completed.
   *
   * @var \Drupal\group\Entity\GroupInterface
   */
  public $itinerary;

  /**
   * The type of action done that made this itinerary completed.
   *
   * @var string
   */
  public $completionAction;

  /**
   * The last relationship that made this itinerary to be autocompleted.
   *
   * @var ?|\Drupal\group\Entity\GroupRelationshipInterface
   */
  public $groupLastRelationshipAutocomplete;

  /**
   * Constructs the event.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account of the user logged in.
   * @param \Drupal\group\Entity\GroupMembershipInterface $group_membership
   *   The membership entity.
   * @param \Drupal\group\Entity\GroupInterface $itinerary
   *   The itinerary group.
   * @param string $completion_action
   *   The name of the completion action.
   * @param \Drupal\group\Entity\GroupRelationshipInterface $last_relationship_autocomplete
   *   The last relationship that fires the autocompletion of itinerary.
   */
  public function __construct(AccountInterface $account, GroupMembershipInterface $group_membership, GroupInterface $itinerary, $completion_action, ?GroupRelationshipInterface $last_relationship_autocomplete = NULL) {
    $this->account = $account;
    $this->groupMembership = $group_membership;
    $this->itinerary = $itinerary;
    $this->completionAction = $completion_action;
    $this->groupLastRelationshipAutocomplete = $last_relationship_autocomplete;
  }

}
