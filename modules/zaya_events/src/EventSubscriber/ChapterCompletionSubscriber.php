<?php

declare(strict_types=1);

namespace Drupal\zaya_events\EventSubscriber;

use Drupal\node\Entity\Node;
use Drupal\zaya\Entity\Node\ZayaProgress;
use Drupal\zaya_events\Event\ChapterCompletionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The subscriber to the event chapter completion.
 */
final class ChapterCompletionSubscriber implements EventSubscriberInterface {

  /**
   * Chapter completion event handler.
   */
  public function onChapterCompletion(ChapterCompletionEvent $event): void {
    $itinerary = $event->groupMembership ? $event->groupMembership->getGroup() : NULL;

    $chapter = $event->groupChapterRelationship ? $event->groupChapterRelationship->getEntity() : NULL;

    $progress_values = [
      "title" => "completion of chapter: {$chapter->label()}",
      "type" => 'zaya_progress',
      "uid" => $event->account->id(),
      "zaya_learning_entity_bundle" => "zaya_chapter",
      "zaya_learning_entity_type" => "node",
      "zaya_event" => $event->completionAction,
      "zaya_progress_status" => ZayaProgress::COMPLETED,
    ];
    if ($itinerary) {
      $progress_values['zaya_itinerary'] = ['entity' => $itinerary];
      $progress_values['zaya_itinerary_uuid'] = $itinerary->uuid();
    }
    if ($chapter) {
      $progress_values['zaya_chapter'] = ['entity' => $chapter];
      $progress_values['zaya_chapter_uuid'] = $chapter->uuid();
      if ($itinerary) {
        $progress_values["title"] = "completion itinerary({$itinerary->label()}) chapter: {$chapter->label()}";
      }
    }
    /*
    if ($resource) {
    $progress_values['zaya_resource'] = ['entity' => $resource];
    $progress_values['zaya_resource_uuid'] = $resource->uuid();
    $progress_values["title"] .= " when ending resource: {$resource->label()}";
    }
     */
    $progress_node = Node::create($progress_values);
    $progress_node->save();
    $event->groupMembership->zaya_progress[] = $progress_node->id();
    $event->groupMembership->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ChapterCompletionEvent::EVENT_NAME => ['onChapterCompletion'],
    ];
  }

}
