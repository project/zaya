<?php

declare(strict_types=1);

namespace Drupal\zaya_events\EventSubscriber;

use Drupal\node\Entity\Node;
use Drupal\zaya\Entity\Node\ZayaProgress;
use Drupal\zaya_events\Event\ResourceCompletionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The subscriber to the event resource completion.
 */
final class ResourceCompletionSubscriber implements EventSubscriberInterface {

  /**
   * Chapter completion event handler.
   */
  public function onResourceCompletion(ResourceCompletionEvent $event): void {
    $itinerary = $event->groupMembership ? $event->groupMembership->getGroup() : NULL;

    $chapter = $event->groupChapterRelationship ? $event->groupChapterRelationship->getEntity() : NULL;
    $resource = $event->groupResourceRelationship ? $event->groupResourceRelationship->getEntity() : NULL;

    $progress_values = [
      "title" => "complete of resource:",
      "type" => 'zaya_progress',
      "uid" => $event->account->id(),
      "zaya_learning_entity_bundle" => "zaya_resource",
      "zaya_learning_entity_type" => "node",
      "zaya_event" => $event->completionAction,
      "zaya_progress_status" => ZayaProgress::COMPLETED,
    ];
    if ($resource) {
      $progress_values['zaya_resource'] = ['entity' => $resource];
      $progress_values['zaya_resource_uuid'] = $resource->uuid();
      $progress_values["title"] .= " {$resource->label()}";
    }

    if ($chapter) {
      $progress_values['zaya_chapter'] = ['entity' => $chapter];
      $progress_values['zaya_chapter_uuid'] = $chapter->uuid();
      $progress_values["title"] .= " in chapter: {$chapter->label()}";
    }

    if ($itinerary) {
      $progress_values['zaya_itinerary'] = ['entity' => $itinerary];
      $progress_values['zaya_itinerary_uuid'] = $itinerary->uuid();
      $progress_values["title"] .= " in itinerary{$itinerary->label()}";
    }
    $progress_node = Node::create($progress_values);
    $progress_node->save();

    // No need to store in dedicated group membership zaya progress field...
    // $event->groupMembership->zaya_progress[] = $progress_node->id();
    // $event->groupMembership->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ResourceCompletionEvent::EVENT_NAME => ['onResourceCompletion'],
    ];
  }

}
