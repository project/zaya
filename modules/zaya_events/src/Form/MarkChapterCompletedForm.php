<?php

declare(strict_types=1);

namespace Drupal\zaya_events\Form;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupMembership;
use Drupal\group\Entity\GroupRelationshipInterface;
use Drupal\zaya\Entity\Node\ZayaProgress;
use Drupal\zaya_events\Event\ChapterCompletionEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Zaya mark as completed button form for chapters.
 */
final class MarkChapterCompletedForm extends FormBase {
  /**
   * The account interacting with the form.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * The user membership in the relationship content context.
   *
   * @var \Drupal\group\Entity\GroupMembershipInterface
   */
  protected $membership;

  /**
   * The current relationship content.
   *
   * @var \Drupal\group\Entity\GroupRelationshipInterface
   */
  protected $relationship;

  /**
   * {@inheritdoc}
   */
  public function __construct(ContainerAwareEventDispatcher $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('event_dispatcher'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'zaya_events_mark_chapter_completed';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?GroupRelationshipInterface $relationship = NULL, ?AccountInterface $account = NULL): array {
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Mark as complete'),
        '#disabled' => TRUE,
        '#attributes' => [
          'aria-describedby' => 'disabledReason',
          'aria-disabled' => 'true',
          'class' => ['zaya-event-mark-chapter-complete'],
        ],
        'disabled_reason' => [
          '#type' => 'container',
          '#id' => 'disabledReason',
          '#attributes' => [
            'role' => 'tooltip',
            'class' => ['must-complete-info'],
          ],
        ],
      ],
    ];
    $group = $relationship->getGroup();
    if ($group->hasPermission('mark chapter as complete', $account) === FALSE) {
      $form['actions']['submit']['#attributes']['class'] = ['hidden'];
      $form['actions']['submit']['disabled_reason']['#markup'] = $this->t('Not allowed to mark as complete');
      return $form;
    }
    $completion = $relationship->getCompletionState();
    if ($completion === ZayaProgress::COMPLETED) {
      $form['actions']['submit']['#attributes']['class'] = ['hidden'];
      $form['actions']['submit']['disabled_reason']['#markup'] = $this->t('Chapter already completed');
      return $form;
    }
    $form['actions']['submit']['disabled_reason']['#markup'] = $this->t('Inner resources already completed');
    $dependency_state = $relationship->getDependenciesState();
    $inner_resources_dependency = $relationship->getResourcesCompletionState();
    // DEBUG dpm($dependency_state === $relationship::DEPENDENCY_UNSATISFIED);.
    if ($dependency_state === $relationship::DEPENDENCY_UNSATISFIED) {
      $form['actions']['submit']['#disabled'] = TRUE;
      $form['actions']['submit']['#attributes']['aria-disabled'] = 'true';
      $form['actions']['submit']['disabled_reason']['#attributes']['class'] = ['must-complete-warning'];
      $form['actions']['submit']['disabled_reason']['#markup'] = $this->t('Must complete previous chapter');
    }
    elseif ($inner_resources_dependency === ZayaProgress::UNCOMPLETED) {
      $form['actions']['submit']['#disabled'] = TRUE;
      $form['actions']['submit']['#attributes']['aria-disabled'] = 'true';
      $form['actions']['submit']['disabled_reason']['#attributes']['class'] = ['must-complete-warning'];
      $form['actions']['submit']['disabled_reason']['#markup'] = $this->t('Must complete the inner resources');
    }
    else {
      $this->account = $account;
      $this->membership = GroupMembership::loadSingle($relationship->getGroup(), $account);
      $this->relationship = $relationship;
      if ($dependency_state === $relationship::DEPENDENCY_UNDEFINED) {
        $form['actions']['submit']['#disabled'] = FALSE;
        $form['actions']['submit']['#value'] = $this->t('Mark as complete');
        $form['actions']['submit']['disabled_reason']['#disabled'] = TRUE;
        $form['actions']['submit']['#attributes']['aria-disabled'] = 'false';
      }
      if ($dependency_state === $relationship::DEPENDENCY_SATISFIED) {
        $form['actions']['submit']['#disabled'] = FALSE;
        $form['actions']['submit']['#value'] = $this->t('Mark as complete');
        $form['actions']['submit']['disabled_reason']['#disabled'] = TRUE;
        $form['actions']['submit']['#attributes']['aria-disabled'] = 'false';
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $relationship = $form_state->getBuildInfo()['args'][0];
    $dependency_state = $relationship->getDependenciesState();
    $inner_resources_dependency = $relationship->getResourcesCompletionState();
    if ($dependency_state === $relationship::DEPENDENCY_UNSATISFIED) {
      $form_state->setErrorByName('submit', $this->t('Must complete the previous chapter'));
    }
    elseif ($inner_resources_dependency === ZayaProgress::UNCOMPLETED) {
      $form_state->setErrorByName('submit', $this->t('Must complete the inner resources'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if ($this->relationship->getGroupId() == $this->membership->getGroupId()) {
      $completion_event = new ChapterCompletionEvent($this->account, $this->membership, $this->relationship/*$match_relationship,*/, "mark completed chapter");
      $this->eventDispatcher->dispatch($completion_event, ChapterCompletionEvent::EVENT_NAME);
      $this->messenger()->addStatus($this->t('The chapter is marked as completed.'));
    }
    // $form_state->setRedirect('<front>');
  }

}
