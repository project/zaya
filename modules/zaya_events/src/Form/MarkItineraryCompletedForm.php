<?php

declare(strict_types=1);

namespace Drupal\zaya_events\Form;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupMembership;
use Drupal\zaya\Entity\Node\ZayaProgress;
use Drupal\zaya_events\Event\ItineraryCompletionEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Zaya mark as completed Itinerary button form.
 */
final class MarkItineraryCompletedForm extends FormBase {
  /**
   * The account which is requesting the form.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * The user membership in the relationship content context.
   *
   * @var \Drupal\group\Entity\GroupMembershipInterface
   */
  protected $membership;

  /**
   * The current relationship content.
   *
   * @var \Drupal\group\Entity\GroupRelationshipInterface
   */
  protected $relationship;

  /**
   * {@inheritdoc}
   */
  public function __construct(ContainerAwareEventDispatcher $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('event_dispatcher'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'zaya_events_mark_itinerary_completed';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?GroupInterface $group = NULL, ?AccountInterface $account = NULL): array {
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Mark as complete'),
        '#disabled' => TRUE,
        '#attributes' => [
          'aria-describedby' => 'disabledReason',
          'aria-disabled' => 'true',
          'class' => ['zaya-event-mark-itinerary-complete'],
        ],
        'disabled_reason' => [
          '#type' => 'container',
          '#id' => 'disabledReason',
          '#attributes' => [
            'role' => 'tooltip',
            'class' => ['must-complete-info'],
          ],
        ],
      ],
    ];
    if ($group->hasPermission('mark itinerary as complete', $account) === FALSE) {
      $form['actions']['submit']['#attributes']['class'] = ['hidden'];
      $form['actions']['submit']['disabled_reason']['#markup'] = $this->t('Not allowed to mark as complete');
      return $form;
    }

    $completion = $group->getItineraryCompletionState();
    if ($completion === ZayaProgress::COMPLETED) {
      $form['actions']['submit']['#attributes']['class'] = ['hidden'];
      $form['actions']['submit']['disabled_reason']['#markup'] = $this->t('Itinerary already completed');
      return $form;
    }
    $inner_chapters_dependency = $group->getChaptersCompletionState();

    if ($inner_chapters_dependency === ZayaProgress::UNCOMPLETED) {
      $form['actions']['submit']['#disabled'] = TRUE;
      $form['actions']['submit']['#attributes']['aria-disabled'] = 'true';
      $form['actions']['submit']['disabled_reason']['#attributes']['class'] = ['must-complete-warning'];
      $form['actions']['submit']['disabled_reason']['#markup'] = $this->t('Must complete the inner chapters');
    }
    else {
      $this->account = $account;
      $this->membership = GroupMembership::loadSingle($group, $account);
      $form['actions']['submit']['#disabled'] = FALSE;
      $form['actions']['submit']['#value'] = $this->t('Mark as complete');
      $form['actions']['submit']['disabled_reason']['#disabled'] = TRUE;
      $form['actions']['submit']['#attributes']['aria-disabled'] = 'false';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // @todo Validate the form here.
    // Example:
    // @code
    //   if (mb_strlen($form_state->getValue('message')) < 10) {
    //     $form_state->setErrorByName(
    //       'message',
    //       $this->t('Message should be at least 10 characters.'),
    //     );
    //   }
    // @endcode
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if ($this->membership->getGroupId()) {
      $completion_event = new ItineraryCompletionEvent(account: $this->account, group_membership: $this->membership, itinerary: $this->membership->getGroup(), completion_action:"mark completed itinerary", last_relationship_autocomplete: NULL);
      $this->eventDispatcher->dispatch($completion_event, ItineraryCompletionEvent::EVENT_NAME);
      $this->messenger()->addStatus($this->t('The itinerary is marked as completed.'));
    }
    // $form_state->setRedirect('<front>');
  }

}
