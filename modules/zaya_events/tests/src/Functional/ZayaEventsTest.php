<?php

declare(strict_types=1);

namespace Drupal\Tests\zaya_events\Functional;

use Drupal\group\PermissionScopeInterface;
use Drupal\Tests\zaya\Functional\ZayaBrowserTestBase;

/**
 * Test description.
 *
 * @group zaya_events_tests
 */
final class ZayaEventsTest extends ZayaBrowserTestBase {

  /**
   * The modules to be enabled.
   *
   * @var modules
   */
  protected static $modules = [
    'zaya_events',
  ];

  /**
   * Test create group content.
   *
   * @dataProvider provideRolesScenarios
   */
  public function testButtonFinishesChapter($scope = '', $global_role = '', $global_permissions = []) {
    $this->createGroupAndOperator($scope, $global_role, $global_permissions);
    $node_and_relationship = $this->createGroupChapter($this->group, ['title' => $this->randomString()]);
    $this->drupalGet("/group/{$this->group->id()}/content/{$node_and_relationship['relationship']->id()}");
    if ($scope == PermissionScopeInterface::OUTSIDER_ID) {
      $this->assertSession()->buildXPathQuery('input#edit-submit[disabled="disabled"]');

    }
    else {
      $this->click('#edit-submit[value="Mark as complete"]');

      $this->assertSession()->pageTextContains('The chapter is marked as completed.');
      $this->assertSession()->pageTextContains('Chapter already completed');
    }
  }

  /**
   * Test itinerary normal completion flow.
   *
   * @dataProvider provideRolesScenarios
   */
  public function testItineraryCompletionFlow($scope = '', $global_role = '', $global_permissions = []) {
    $this->createGroupAndOperator($scope, $global_role, $global_permissions);
    $itinerary_chapter_relationships = $this->createItineraryWithContent($this->group);
    $this->drupalGet("/group/{$this->group->id()}/content");
    if ($this->loggedInUser && $this->group->hasPermission('mark chapter as complete', $this->loggedInUser)) {
      foreach ($itinerary_chapter_relationships as $itinerary_chapter_relationship) {
        $this->drupalGet("/group/{$this->group->id()}/content/{$itinerary_chapter_relationship->id()}");
        // Must check if the first could be completed.
        if ($itinerary_chapter_relationship->getPluginId() === 'group_node:zaya_chapter') {
          // If chapter contains resources, those are not completed, so cannot
          // mark chapter as complete.
          if ($itinerary_chapter_relationship->getEntity()->zaya_chapter_resources->count() > 0) {
            $this->assertSession()->pageTextContains('Must complete the inner resources');
            // Let's complete the resources.
            foreach ($itinerary_chapter_relationship->getEntity()->zaya_chapter_resources->referencedEntities() as $chapter_resource_node) {
              $chapter_resource_relationships = $this->group->getRelationshipsByEntity($chapter_resource_node, 'group_node:zaya_resource');
              if (count($chapter_resource_relationships) > 0) {
                $chapter_resource_relationship = $chapter_resource_relationships[array_key_first($chapter_resource_relationships)];
                $this->drupalGet("/group/{$this->group->id()}/content/{$chapter_resource_relationship->id()}");
                $this->assertSession()->buttonExists('Mark as complete');
                $this->click('#edit-submit.zaya-event-mark-resource-complete');
                $this->drupalGet("/group/{$this->group->id()}/content/{$chapter_resource_relationship->id()}");
                // Revisit resource relationship to assert form is disabled.
                $this->assertSession()->buildXPathQuery('input#edit-submit.zaya-event-mark-resource-complete[disabled="disabled"]');
              }
            }
            // Revisit chapter relationship with resources completed.
            $this->drupalGet("/group/{$this->group->id()}/content/{$itinerary_chapter_relationship->id()}");
          }
          // If getting here resources are completed or they don't exist, so
          // let's mark chapter as completed.
          $this->click('#edit-submit.zaya-event-mark-chapter-complete');
          // Revisit the chapter relationship to assert is completed.
          $this->drupalGet("/group/{$this->group->id()}/content/{$itinerary_chapter_relationship->id()}");
          $this->assertSession()->buildXPathQuery('input#edit-submit.zaya-event-mark-chapter-complete[disabled="disabled"]');
        }

      }
      // If getting here chapters are completed or they don't exist, so
      // let's mark the itinerary as completed.
      $this->drupalGet("/group/{$this->group->id()}");
      if ($this->group->hasPermission('mark itinerary as complete', $this->loggedInUser)) {
        $this->assertSession()->buttonExists('Mark as complete');
        $this->click('#edit-submit.zaya-event-mark-itinerary-complete[value="Mark as complete"]');
        // Revisit to check if completed.
        $this->drupalGet("/group/{$this->group->id()}");
        $this->assertSession()->pageTextContains('Itinerary already completed');
        // @todo check progress!!!
      }
      else {
        $this->assertSession()->buildXPathQuery('input#edit-submit.zaya-event-mark-itinerary-complete[disabled="disabled"]');
      }
    }
    else {
      // Not authorized to complete chapters.
      $this->assertSession()->pageTextContains('You are not authorized to access this page.');
      // Game over.
    }
  }

  /**
   * Test create group content with content editor.
   *
   * @todo This method needs to be checked completed.
   */
  public function todoTestCreateGroupChapter() {
    $this->group = $this->createGroup(['type' => 'zaya_itinerary', 'uid' => $this->loggedInUser->id()]);
    // , TRUE);
    $this->user = $this->createUser([], "user_content_editor_global_role" . $this->randomMachineName(4), values: ['roles' => ['content_editor']]);
    $this->group->addMember($this->user);
    $this->drupalLogin($this->user);

    $this->drupalGet("/group/{$this->group->id()}/content/create/group_node:zaya_chapter");
    $this->assertSession()->statusCodeEquals(200);

    $submit_button = 'Save';
    $this->assertSession()->buttonExists($submit_button);
    $name = $this->randomString();
    $body = $this->randomMachineName(16);
    $edit = [
      'title[0][value]' => $name,
      'body[0][value]' => $body,
    ];

    $this->submitForm($edit, $submit_button);
    $this->assertSession()->pageTextMatchesCount(1, '/Chapter (.*) has been created./');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Provide Insider roles.
   */
  public function provideRolesScenarios() {
    return [
      [PermissionScopeInterface::INSIDER_ID, 'administrator', []],
      [PermissionScopeInterface::INSIDER_ID, 'content_editor', []],
      [PermissionScopeInterface::OUTSIDER_ID, 'content_editor', []],
      [PermissionScopeInterface::OUTSIDER_ID, 'authenticated', []],
      [PermissionScopeInterface::OUTSIDER_ID, 'anonymous', []],
    ];
  }

}
