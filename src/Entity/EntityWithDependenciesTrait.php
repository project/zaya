<?php

namespace Drupal\zaya\Entity;

use Drupal\group\Entity\GroupRelationshipInterface;
use Drupal\zaya\Entity\Node\ZayaProgress;

/**
 * Trait to provide methods and props to entities with dependencies.
 */
trait EntityWithDependenciesTrait {
  // Possible since php8.2
  // public const DEPENDENCY_UNDEFINED = -1;
  // public const DEPENDENCY_SATISFIED = 1;
  // public const DEPENDENCY_UNSATISFIED = 0;.
  /**
   * Dependency is not defined.
   *
   * @var int
   */
  public static int $dependencyUndefined = -1;
  /**
   * Dependency is satisfied.
   *
   * @var int
   */
  public static int $dependencySatisfied = 1;
  /**
   * Dependency is unsatisfied.
   *
   * @var int
   */
  public static int $dependencyUnsatisfied = 0;

  /**
   * Gets the dependency state of the relationship chapter.
   *
   * (chapter)
   *
   * @return int
   *   DEPENDENCY_UNDEFINED, DEPENDENCY_SATISFIED or DEPENDENCY_UNSATISFIED
   */
  public function getDependenciesState(): int {
    $dependency = $this->get('zaya_dependency');
    if (!$dependency->entity) {
      return $this::$dependencyUndefined;
    }
    $completed_chapters = \Drupal::entityTypeManager()->getStorage('node')
      ->getQuery()
      ->condition('uid', \Drupal::currentUser()->id())
      ->condition('zaya_chapter', $dependency->entity->id())
      ->condition('type', 'zaya_progress')
      ->condition('zaya_progress_status', ZayaProgress::COMPLETED)
      ->accessCheck()
      ->count()
      ->execute();

    return ($completed_chapters > 0) ? $this::$dependencySatisfied : $this::$dependencyUnsatisfied;
  }

  /**
   * Get the relationships from the dependency entity reference field.
   *
   * (chapter)
   *
   * @returns Array|null
   *   Array of the dependency relationships
   */
  public function getDependencyNodeRelationships(): ?array {
    $group = $this->getGroup();
    $dependency = $this->get('zaya_dependency');
    if (!$dependency->entity) {
      return [];
    }
    return $group->getRelationshipsByEntity($dependency->entity);
  }

  /**
   * Get the dependents relationships.
   *
   * Gets the dependent relationships pointing to this entity via dependency
   * entity reference field.
   *
   * (chapter)
   *
   * @returns Array
   *   Array of the dependency relationships
   */
  public function getDependentNodeRelationships(): array {
    $group = $this->getGroup();
    $storage = \Drupal::entityTypeManager()->getStorage('group_relationship');
    $query = $storage
      ->getQuery()
      ->condition('zaya_dependency', $this->getEntity()->id())
      ->condition('plugin_id', 'group_node:zaya_chapter')
      ->condition('gid', $group->id())
      ->accessCheck();

    $dependent_relationships = [];
    $relationships = $storage->loadMultiple($query->execute());
    foreach ($relationships as $relationship) {
      assert($relationship instanceof GroupRelationshipInterface);
      // @todo cache as in https://git.drupalcode.org/project/group/-/blob/3.3.x/src/Entity/GroupMembershipTrait.php
      $dependent_relationships[] = $relationship;
    }
    return $dependent_relationships;
  }

}
