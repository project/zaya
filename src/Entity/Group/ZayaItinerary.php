<?php

declare(strict_types=1);

namespace Drupal\zaya\Entity\Group;

use Drupal\group\Entity\Group;
use Drupal\zaya\Entity\Node\ZayaProgress;

/**
 * A bundle class for group entities.
 */
final class ZayaItinerary extends Group {
  use \Drupal\zaya\Entity\EntityWithCompletionStateTrait;

  // Move to EntityWithCompletionStateTrait when deprecating php8.1.
  public const DEPENDENCY_UNDEFINED = -1;
  public const DEPENDENCY_SATISFIED = 1;
  public const DEPENDENCY_UNSATISFIED = 0;

  /**
   * Gets the completion state of the related chapters completed.
   *
   * (group)
   *
   * @returns int
   *   The integer value based on completion status: ZayaProgress::COMPLETED
   *   or ZayaProgress::UNCOMPLETED
   */
  public function getChaptersCompletionState() {
    $chapters = $this->getRelationships('group_node:zaya_chapter');
    if (count($chapters) == 0) {
      // No chapters for this group defined.
      return ZayaProgress::COMPLETED;
    }
    $node_query = \Drupal::entityTypeManager()->getStorage('node')->getQuery();
    $nids_revs_conditions = $node_query->orConditionGroup();
    foreach ($chapters as $chapter) {
      $nid_rev_cond = $node_query->andConditionGroup()
        ->condition('zaya_chapter.target_id', $chapter->getEntityId())
        // If it is completed in other itinerary it is completed here too
        // ->condition('zaya_itinerary.target_id', $this->id())
        // ->condition('zaya_resource.target_revision_id',
        // $resource->entity->getRevisionId(), "<=")
        // Take revision in account so will pass as completed the ones in
        // progress that has an equal or greater than the resource current
        // revision.
        ->condition('uid', \Drupal::currentUser()->id())
        ->condition('type', 'zaya_progress')
        ->condition('zaya_progress_status', ZayaProgress::COMPLETED);
      $nids_revs_conditions->condition($nid_rev_cond);
    }
    $completed_chapters_query = $node_query
      ->condition($nids_revs_conditions)
      ->accessCheck();
    // DEBUG
    // dpm((string)$completed_chapters_query);.
    $completed_chapters_count = $completed_chapters_query
      ->count()
      ->execute();
    // dpm("{$completed_chapters_count} / " . count($chapters));
    return (count($chapters) <= $completed_chapters_count) ? ZayaProgress::COMPLETED : ZayaProgress::UNCOMPLETED;
  }

  /**
   * Gets the completion progress of the related chapters completed.
   *
   * (itinerary)
   *
   * @returns array
   *   With 'completed' key the count of completed chapters and 'uncompleted'
   *   for the count of uncompleted.
   */
  public function getItineraryCompletionProgress(): array {
    $chapters = $this->getRelationships('group_node:zaya_chapter');
    if (count($chapters) == 0) {
      // No chapters for this itinerary defined.
      return ['completed' => 1, "uncompleted" => 1];
    }
    $completed = 0;
    $uncompleted = 0;
    foreach ($chapters as $chapter) {
      $chapter_completion_state = $chapter->getCompletionState();
      if ($chapter_completion_state === ZayaProgress::COMPLETED) {
        ++$completed;
      }
      elseif ($chapter_completion_state === ZayaProgress::UNCOMPLETED) {
        ++$uncompleted;
      }
    }
    return ['completed' => $completed, 'uncompleted' => $uncompleted];
  }

}
