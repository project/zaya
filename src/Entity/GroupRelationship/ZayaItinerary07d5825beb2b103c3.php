<?php

declare(strict_types=1);

namespace Drupal\zaya\Entity\GroupRelationship;

use Drupal\group\Entity\GroupRelationship;
use Drupal\zaya\Entity\Node\ZayaProgress;

/**
 * A bundle class for group_relationship chapter entities.
 */
final class ZayaItinerary07d5825beb2b103c3 extends GroupRelationship {
  use \Drupal\zaya\Entity\EntityWithDependenciesTrait;
  use \Drupal\zaya\Entity\EntityWithCompletionStateTrait;
  use \Drupal\zaya\Entity\RelationshipWithCompletionStateTrait;

  // Move to EntityWithCompletionStateTrait when deprecating php8.1.
  public const DEPENDENCY_UNDEFINED = -1;
  public const DEPENDENCY_SATISFIED = 1;
  public const DEPENDENCY_UNSATISFIED = 0;

  /**
   * Get the resources mapped to the group relationships from entity ref field.
   *
   * (chapter)
   *
   * @returns Array|null
   *   Array of the resources relationships
   */
  public function getResourcesAsRelationships(): ?Array {
    $group = $this->getGroup();
    $resources = $this->getEntity()->get('zaya_chapter_resources');
    $resources_relationships = [];
    foreach ($resources as $resource) {
      $resources_relationship = $group->getRelationshipsByEntity($resource->entity);
      if (!$resources_relationship) {
        continue;
      }
      $resources_relationships[] = array_pop($resources_relationship);
    }
    return $resources_relationships;
  }

  /**
   * Gets the completion state of the related resources completed.
   *
   * (chapter)
   *
   * @returns int
   *   The integer value based on completion status: ZayaProgress::COMPLETED or
   *   ZayaProgress::UNCOMPLETED
   */
  public function getResourcesCompletionState(): int {
    $resources = $this->getEntity()->zaya_chapter_resources;
    if (count($resources) == 0) {
      // No resources for this chapter defined.
      return ZayaProgress::COMPLETED;
    }
    $node_query = \Drupal::entityTypeManager()->getStorage('node')->getQuery();
    $nids_revs_conditions = $node_query->orConditionGroup();
    foreach ($resources as $resource) {
      $nid_rev_cond = $node_query->andConditionGroup()
        ->condition('zaya_resource.target_id', $resource->entity->id())
        // ->condition('zaya_resource.target_revision_id',
        // $resource->entity->getRevisionId(), "<=")
        // Take revision in account so will pass as completed the ones in
        // progress that has an equal or greater than the resource current
        // revision.
        ->condition('uid', \Drupal::currentUser()->id())
        ->condition('type', 'zaya_progress')
        ->condition('zaya_progress_status', ZayaProgress::COMPLETED);
      $nids_revs_conditions->condition($nid_rev_cond);

    }
    $completed_resources_query = $node_query
      ->condition($nids_revs_conditions)
      ->accessCheck();
    // DEBUG dpm((string)$completed_resources_query);.
    $completed_resources_count = $completed_resources_query
      ->count()
      ->execute();
    // DEBUG dpm("{$completed_resources_count} / " . count($resources));
    return (count($resources) <= $completed_resources_count) ? ZayaProgress::COMPLETED : ZayaProgress::UNCOMPLETED;
  }

}
