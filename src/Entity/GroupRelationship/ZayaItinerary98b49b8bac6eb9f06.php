<?php

declare(strict_types=1);

namespace Drupal\zaya\Entity\GroupRelationship;

use Drupal\group\Entity\GroupRelationship;
use Drupal\node\Entity\Node;

/**
 * A bundle class for group_relationship resource entities.
 */
final class ZayaItinerary98b49b8bac6eb9f06 extends GroupRelationship {
  use \Drupal\zaya\Entity\EntityWithCompletionStateTrait;
  use \Drupal\zaya\Entity\RelationshipWithCompletionStateTrait;

  // Move to EntityWithCompletionStateTrait when deprecating php8.1.
  public const DEPENDENCY_UNDEFINED = -1;
  public const DEPENDENCY_SATISFIED = 1;
  public const DEPENDENCY_UNSATISFIED = 0;

  /**
   * Get parent dependency state to grant access to current chapter resources.
   *
   * @return int
   *   \Drupal\zaya\Entity\ZayaItinerary07d5825beb2b103c3::DEPENDENCY_UNDEFINED|
   *   \Drupal\zaya\Entity\ZayaItinerary07d5825beb2b103c3::DEPENDENCY_SATISFIED|
   *   \Drupal\zaya\Entity\ZayaItinerary07d5825beb2b103c3::DEPENDENCY_UNSATISFIED
   */
  public function getParentChapterDependenciesState(): int {
    $group = $this->getGroup();
    // Get the chapters with this resource in its zaya_chapter_resources field.
    $chapters_ids_with_resource = \Drupal::entityTypeManager()->getStorage('node')
      ->getQuery()
      ->condition('zaya_chapter_resources', $this->getEntity()->id(), 'IN')
      ->condition('type', 'zaya_chapter')
      ->accessCheck()
      ->execute();
    $chapters_with_resource = Node::loadMultiple($chapters_ids_with_resource);
    $unsatisfied = FALSE;
    foreach ($chapters_with_resource as $chapter_with_resource) {
      // We are only getting resource parent chapters in this group scope.
      $chapters_relationship = $group->getRelationshipsByEntity($chapter_with_resource);
      foreach ($chapters_relationship as $chapter_relationship) {
        $chapter_relationship_dependencies_state = $chapter_relationship->getDependenciesState();
        if ($chapter_relationship_dependencies_state === $chapter_relationship::DEPENDENCY_SATISFIED) {
          return ZayaItinerary07d5825beb2b103c3::DEPENDENCY_SATISFIED;
        }
        elseif ($chapter_relationship_dependencies_state === $chapter_relationship::DEPENDENCY_UNDEFINED) {
          return ZayaItinerary07d5825beb2b103c3::DEPENDENCY_UNDEFINED;
        }
        // Give the chance that other chapters somewhere else could return
        // SATISFIED or UNDEFINED state before returning UNSATISFIED.
        elseif ($chapter_relationship_dependencies_state === $chapter_relationship::DEPENDENCY_UNSATISFIED) {
          $unsatisfied = ZayaItinerary07d5825beb2b103c3::DEPENDENCY_UNSATISFIED;
        }
      }
    }
    // It may be undefined if no chapters there.
    return ($unsatisfied) ?: ZayaItinerary07d5825beb2b103c3::DEPENDENCY_UNDEFINED;
  }

}
