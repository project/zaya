<?php

declare(strict_types=1);

namespace Drupal\zaya\Entity\Node;

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * A bundle class for node entities.
 */
final class ZayaProgress extends Node implements NodeInterface {
  public const COMPLETED = 1;
  public const UNCOMPLETED = 0;

  /**
   * Counts the attempts that the same learning entity has been registered.
   */
  public function getAttempts(): int {
    $storage = $this->entityTypeManager()
      ->getStorage($this->entityTypeId);
    $attempts = $storage->loadByProperties([
      'uid' => $this->getOwner()->id(),
      // Counts without opining about ancestors like chapter or itinerary.
      'zaya_learning_entity_bundle' => $this->zaya_learning_entity_bundle->value,
      'zaya_learning_entity_type' => $this->zaya_learning_entity_type->value,
      "{$this->zaya_learning_entity_bundle->value}" => $this->get($this->zaya_learning_entity_bundle->value)->target_id,
      'zaya_event' => 'visit chapter',
    ]);
    return count($attempts);
  }

}
