<?php

namespace Drupal\zaya\Entity;

use Drupal\zaya\Entity\Node\ZayaProgress;

/**
 * Trait to provide methods and props to relationships with completion state.
 */
trait RelationshipWithCompletionStateTrait {
  /**
   * Possible since php8.2.
   *
   * Public const DEPENDENCY_UNDEFINED = -1;
   * public const DEPENDENCY_SATISFIED = 1;
   * public const DEPENDENCY_UNSATISFIED = 0;
   */

  /**
   * Gets the completion state of the entity based on the state of itself.
   *
   * @returns int
   *   The integer value based on completion status: ZayaProgress::COMPLETED
   *   or ZayaProgress::UNCOMPLETED
   */
  public function getCompletionState(): int {
    $completed_entities = \Drupal::entityTypeManager()->getStorage('node')
      ->getQuery()
      ->condition('uid', \Drupal::currentUser()->id())
      ->condition($this->getEntity()->bundle(), $this->getEntity()->id())
      ->condition('type', 'zaya_progress')
      ->condition('zaya_learning_entity_type', 'node')
      ->condition('zaya_progress_status', ZayaProgress::COMPLETED)
      ->accessCheck()
      ->count()
      ->execute();
    return ($completed_entities > 0) ? ZayaProgress::COMPLETED : ZayaProgress::UNCOMPLETED;
  }

}
