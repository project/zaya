<?php

namespace Drupal\zaya\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\ResettableStackedRouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Group Relationship Type' condition.
 *
 * @Condition(
 *   id = "group_relationship_type",
 *   label = @Translation("Group relationship type"),
 * )
 */
class GroupRelationshipType extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Creates a new GroupRelationshipType instance.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Routing\ResettableStackedRouteMatchInterface $currentRouteMatch
   *   The curretn route match service.
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(EntityStorageInterface $entity_storage, ResettableStackedRouteMatchInterface $currentRouteMatch, array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityStorage = $entity_storage;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager')->getStorage('group_relationship_type'),
      $container->get('current_route_match'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [];

    // Build a list of group type labels.
    $group_types = $this->entityStorage->loadMultiple();
    foreach ($group_types as $type) {
      $options[$type->id()] = $type->label();
    }

    // Show a series of checkboxes for group type selection.
    $form['group_relationship_types'] = [
      '#title' => $this->t('Group relationship types'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->configuration['group_relationship_types'],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['group_relationship_types'] = array_filter($form_state->getValue('group_relationship_types'));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $group_relationship_types = $this->configuration['group_relationship_types'];

    // Format a pretty string if multiple group relationship
    // types were selected.
    if (count($group_relationship_types) > 1) {
      $last = array_pop($group_relationship_types);
      $group_relationship_types = implode(', ', $group_relationship_types);
      return $this->t('The group type is @group_relationship_types or @last', [
        '@group_relationships_types' => $group_relationship_types,
        '@last' => $last,
      ]);
    }

    // If just one was selected, return a simpler string.
    return $this->t('The group type is @group_relationship_type', ['@group_relationship_type' => reset($group_relationship_types)]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    // If there are no group relationship types selected and the condition
    // is not negated, we return TRUE because it means all group types are
    // valid.
    if (empty($this->configuration['group_relationship_types']) && !$this->isNegated()) {
      return TRUE;
    }

    // @todo check if has permission to access these bundles in group
    $group_relationship_type = $this->currentRouteMatch->getParameter('group_relationship');

    // If user hasn't access the parameter isn't set.
    if (!$group_relationship_type) {
      return FALSE;
    }
    // Return a boolean based on the type of relationship of the group.
    return !empty($this->configuration['group_relationship_types'][$group_relationship_type->bundle()]);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['group_relationship_types' => []] + parent::defaultConfiguration();
  }

}
