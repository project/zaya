<?php

declare(strict_types=1);

namespace Drupal\Tests\zaya\Functional;

use Drupal\group\PermissionScopeInterface;

/**
 * Test description.
 *
 * @group zaya_itinerary_tests
 */
final class ZayaItineraryTest extends ZayaBrowserTestBase {

  /**
   * CreateItinerary.
   *
   * @dataProvider provideRolesScenarios
   */
  public function testCreateItinerary($scope = "", $global_role = "", $global_permissions = []) {
    $this->createGroupAndOperator($scope, $global_role, $global_permissions);

    $this->drupalGet("/group/add/zaya_itinerary");
    if (($scope == PermissionScopeInterface::INSIDER_ID || $scope == PermissionScopeInterface::OUTSIDER_ID) && ($global_role == 'administrator')) {
      $this->assertSession()->statusCodeEquals(200);
    }
    else {
      $this->assertSession()->statusCodeEquals(403);
      return;
    }

    $submit_button = 'Create Itinerary';
    $this->assertSession()->buttonExists($submit_button);

    $edit = ['Title' => $this->randomString()];
    $this->submitForm($edit, $submit_button);

  }

  /**
   * Test create group content.
   *
   * @dataProvider provideRolesScenarios
   */
  public function testCreateGroupContent($scope = '', $global_role = '', $global_permissions = []) {
    $this->createGroupAndOperator($scope, $global_role, $global_permissions);
    // $this->drupalGet("/node/add");
    // $this->drupalGet("/group/{$this->group->id()}");
    // $this->drupalGet("/group/{$this->group->id()}/node/create");
    $this->drupalGet("/group/{$this->group->id()}/content/create/group_node:zaya_chapter");
    if (($scope == PermissionScopeInterface::INSIDER_ID || $scope == PermissionScopeInterface::OUTSIDER_ID) && ($global_role == 'content_editor' || $global_role == 'administrator')) {
      $this->assertSession()->statusCodeEquals(200);
    }
    else {
      $this->assertSession()->statusCodeEquals(403);
      return;
    }
    $submit_button = 'Save';
    $this->assertSession()->buttonExists($submit_button);
    $name = $this->randomString();
    $body = $this->randomMachineName(16);
    $edit = [
      'title[0][value]' => $name,
      'body[0][value]' => $body,
    ];

    $this->submitForm($edit, $submit_button);
    $this->assertSession()->pageTextMatchesCount(1, '/Chapter (.*) has been created./');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test create group content with content editor.
   */
  public function testCreateGroupChapter() {
    $this->group = $this->createGroup(['type' => 'zaya_itinerary', 'uid' => $this->loggedInUser->id()]);
    // , TRUE);
    $this->user = $this->createUser([], "user_content_editor_global_role" . $this->randomMachineName(4), values: ['roles' => ['content_editor']]);
    $this->group->addMember($this->user);
    $this->drupalLogin($this->user);

    $this->drupalGet("/group/{$this->group->id()}/content/create/group_node:zaya_chapter");
    $this->assertSession()->statusCodeEquals(200);

    $submit_button = 'Save';
    $this->assertSession()->buttonExists($submit_button);
    $name = $this->randomString();
    $body = $this->randomMachineName(16);
    $edit = [
      'title[0][value]' => $name,
      'body[0][value]' => $body,
    ];

    $this->submitForm($edit, $submit_button);
    $this->assertSession()->pageTextMatchesCount(1, '/Chapter (.*) has been created./');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests user access to chapter.
   *
   * @dataProvider provideRolesScenarios
   */
  public function testUserChapterAccess($global_role = '', $global_permissions = [], $group_permissions = []) {
    $this->group = $this->createGroup(['type' => 'zaya_itinerary']);
    $node_and_relationship = $this->createGroupChapter($this->group, node_values: ['title' => $this->randomString()]);
    $user = $this->createUser(name:"user_authenticated_global_role", values: ['roles' => ['authenticated']]);
    $this->group->addMember($user);
    $this->drupalLogin($user);
    $this->drupalGet("/node/{$node_and_relationship['node']->id()}");
  }

  /**
   * Tests local action links.
   *
   * @dataProvider provideRolesScenarios
   */
  public function testLocalActionLinks($scope = '', $global_role = '', $global_permissions = [], $group_permissions = []) {
    $this->createGroupAndOperator($scope, $global_role, $global_permissions);
    $this->drupalGet("/group/{$this->group->id()}/nodes");
    if ($global_role != 'authenticated' and
     ($global_role != 'anonymous')
    ) {
      $this->assertSession()->statusCodeEquals(200);
    }
    else {
      $this->assertSession()->statusCodeEquals(403);
    }
    $this->drupalGet("/group/{$this->group->id()}/node/create");
    if ($global_role != 'authenticated' and
     ($global_role != 'anonymous')
    ) {
      $this->assertSession()->statusCodeEquals(200);
    }
    else {
      $this->assertSession()->statusCodeEquals(403);
    }

  }

  /**
   * Provide Insider roles.
   */
  public function provideRolesScenarios() {
    return [
      [PermissionScopeInterface::INSIDER_ID, 'administrator', []],
      [PermissionScopeInterface::INSIDER_ID, 'content_editor', []],
      [PermissionScopeInterface::OUTSIDER_ID, 'content_editor', []],
      [PermissionScopeInterface::OUTSIDER_ID, 'authenticated', []],
      [PermissionScopeInterface::OUTSIDER_ID, 'anonymous', []],
    ];
  }

}
